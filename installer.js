/*
PWA App Installer for iPadOS
Copyright (C) 2020 Felix v. Oertzen
pwaapp@von-oertzen-berlin.de

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

iPadOS is a trademark of Apple Inc., registered in the U.S. and other countries.*/
$(document).ready(function() {
  $('#colorpicker').farbtastic('#color');
});
$('#appform').submit(function() {
    $("#color").val($("#color").val().slice(1, $("#color").val().length));
    $("#lname").val($("#lname").val().replace(/\/$/, ""));
    return true; // return false to cancel form action
});

document.addEventListener('DOMContentLoaded', function () {

  document.getElementById('WA')
    .addEventListener('click', function() {
      $('#fname').val("WhatsApp");
      $('#lname').val("https://web.whatsapp.com");
      $('#iname').val("");
      $('#color').val("#445a62");
      $.farbtastic('#colorpicker').setColor("#445a62");
    });

    document.getElementById('IG')
      .addEventListener('click', function() {
        $('#fname').val("Instagram");
        $('#lname').val("https://www.instagram.com");
        $('#iname').val("");
        $('#color').val("#ffffff");
        $.farbtastic('#colorpicker').setColor("#ffffff");
      });
    document.getElementById('DC')
      .addEventListener('click', function() {
        $('#fname').val("Discord");
        $('#lname').val("https://discord.com/login");
        $('#iname').val("");
        $('#color').val("#35393e");
        $.farbtastic('#colorpicker').setColor("#35393e");
      });
    document.getElementById('TW')
      .addEventListener('click', function() {
        $('#fname').val("Twitter");
        $('#lname').val("https://www.twitter.com");
        $('#iname').val("");
        $('#color').val("#00a3ef");
        $.farbtastic('#colorpicker').setColor("#00a3ef");
      });
    document.getElementById('YT')
      .addEventListener('click', function() {
        $('#fname').val("YouTube");
        $('#lname').val("https://www.youtube.com");
        $('#iname').val("https://www.youtube.com/img/desktop/yt_1200.png");
        $('#color').val("#FFFFFF");
        $.farbtastic('#colorpicker').setColor("#FFFFFF");
      });
});
