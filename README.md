![License](https://img.shields.io/badge/License-GNU%20Affero%20General%20Public%20License%20v3.0-green.svg?logo=gnu)
![HTML5](https://img.shields.io/badge/using-HTML5-black.svg?logo=html5)
![CSS3](https://img.shields.io/badge/using-CSS3-yellow.svg?logo=css3)

# PWA Installer for iPadOS
# General
This page enables you to install WhatsApp Web, Instagram and Co. as an app on iPadOS.
There are several reasons for this:

* not every platform provides an iPadOS app
* you just want to set up multiple accounts on one device
* web apps save your privacy because the interaction with your device is limited like in Safari

# How to use?

Just clone this project to a directory on your webspace and rename the file `htaccess`to `.htaccess`.

# third party libraries

* [farbtastic](https://github.com/mattfarina/farbtastic) 1.2 by Steven Wittens under GPLv2
* [jQuery](https://jquery.org/) 3.5.1 under MIT License
